# Contributor: Siva Mahadevan <me@svmhdvn.name>
# Maintainer: Siva Mahadevan <me@svmhdvn.name>
pkgname=kakoune-lsp
pkgver=18.0.3
pkgrel=0
pkgdesc="Kakoune Language Server Protocol Client"
url="https://github.com/kakoune-lsp/kakoune-lsp"
arch="all !s390x !riscv64" # limited by cargo
license="Unlicense OR MIT"
makedepends="cargo cargo-auditable"
source="$pkgname-$pkgver.tar.gz::https://github.com/kakoune-lsp/kakoune-lsp/archive/refs/tags/v$pkgver.tar.gz"


prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen --release
}

package() {
	install -Dvm755 target/release/kak-lsp -t "$pkgdir"/usr/bin/
}

sha512sums="
28107c14b63354968deff79716aa54ed05ffa1dc187cd9a388c0bdf68ae0f58932611ba96cfa6d7ad0f8c36354b34b0ea9dc484ada4254897ae1185ebc115803  kakoune-lsp-18.0.3.tar.gz
"
