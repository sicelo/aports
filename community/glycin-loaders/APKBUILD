# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=glycin-loaders
pkgver=1.0.1
pkgrel=3
pkgdesc="Sandboxed and extendable image decoding"
url="https://gitlab.gnome.org/sophie-h/glycin"
# s390x: https://github.com/nix-rust/nix/issues/1968
# loongarch64: libc crate fails to build
arch="all !s390x !loongarch64"
license="MPL-2.0 OR LGPL-2.0-or-later"
depends="bubblewrap"
makedepends="meson cargo clang16-dev gtk4.0-dev libheif-dev libjxl-dev libseccomp-dev"
source="https://download.gnome.org/sources/glycin-loaders/${pkgver%.*}/glycin-loaders-$pkgver.tar.xz
	libheif-rs-no-bindgen.patch"
options="!check" # tests hang
provides="glycin=$pkgver-r$pkgrel" # for backward compatibility
replaces="glycin" # for backward compatibility

build() {
	abuild-meson \
		-Dtest_skip_install=true \
		. output
	meson compile -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
6e5c02c8f04b130dbfc1e27a1db10bb23c815dcc35cb1623344b2f53ed8845cbb7af5142a826a2276d388723a2503e8e304adff1963baf886e316e122d114609  glycin-loaders-1.0.1.tar.xz
b14fc70ef5067a02286c2866d26879c4ee7d5ac31486cebc7a26266f06553bb27a8326663dbbe469921668dc6f6fc1a781ee5c2c1e1b7fd10e52650309e42ed1  libheif-rs-no-bindgen.patch
"
